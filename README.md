# rich-text-problem

> rich text

## Build Setup

## 问题：支付宝富文本不显示

``` bash
# 安装依赖
yarn install
或者
npm i

# 开发时构建
npm run dev

# 打包构建
npm run build

# 指定平台的开发时构建(微信、支付宝)
npm run dev:wx
npm run dev:my

# 指定平台的打包构建
npm run build:wx
npm run build:my

# 生成 bundle 分析报告
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
